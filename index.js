const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

const app = express();
const PORT = 3006;


// Middlewares
app.use(express.json())
app.use(express.urlencoded({extended:true}))


// Mongoose connection
	// mongoose.connect(<connection string>, {options})
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})

// DB connection notification
	// use connection property of mongoose
const db = mongoose.connection
db.on("error", console.error.bind(console, 'connection error:'))
db.once("open", () => console.log(`Connected to Database`))





// SCHEMA

// Create a Schema for tasks
	//schema determines the structure of the documents to be written in the database
	//schema acts as a blueprint to our data
const taskSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, `Name is required`]
		},
		status: {
			type: String,
			default: "pending"
		}
	}
)

// Create a model out of the schema
	// syntax: mongoose.model(<name>, <schema came from>)
const Task = mongoose.model(`Task`, taskSchema)
	//model is a programming interface that enables us to query and manipulate database using its methods






// ROUTES
// Business Logic for Creating a task
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return a message `Duplicate Task found`
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
app.post('/tasks', (req, res) => {
	// console.log(req.body)	//object

	Task.findOne({name: req.body.name}).then( (result, err) => {
		console.log(result)	//a document

		// If the task already exists in the database, we return a message `Duplicate Task found`
		if(result != null && result.name == req.body.name){
			return res.send(`Duplicate Task found`)

		} else {

			// If the task doesn't exist in the database, we add it in the database


			 // Create a new Task object with a "name" field/property
			let newTask = new Task({
				name: req.body.name
			})

			//use save() method to insert the new document in the database
			newTask.save().then((savedTaks, err) => {
				// console.log(savedTaks)
				if(savedTaks){
					return res.send(savedTaks)
				} else {
					return res.status(500).send(err)
				}
			})
		}
	})
})




// Business Logic for getting all the tasks
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
app.get('/tasks', async (req, res) => {

	await Task.find().then( (result, err) => {
		if(result){
			return res.status(200).send(result)
		} else {
			return res.status(500).json(err)
		}
	})

	//or

	// Task.find({}, (docs, err) => {
	// 	if(docs){
	// 		return res.status(200).send(docs)
	// 	} else {
	// 		return res.status(500).json(err)
	// 	}
	// })
})






app.listen(PORT, () => console.log(`Server connected at port ${PORT}`))
